#ifndef __SDCARD__H__
#define __SDCARD__H__

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"

#ifdef CONFIG_IDF_TARGET_ESP32
#define PIN_MOSI GPIO_NUM_15
#define PIN_MISO GPIO_NUM_2
#define PIN_SCLK GPIO_NUM_14
#define PIN_CS GPIO_NUM_13
#elif CONFIG_IDF_TARGET_ESP32S2
#define PIN_MOSI GPIO_NUM_35
#define PIN_MISO GPIO_NUM_37
#define PIN_SCLK GPIO_NUM_36
#define PIN_CS GPIO_NUM_34
#elif CONFIG_IDF_TARGET_ESP32S3
#define PIN_MOSI GPIO_NUM_35
#define PIN_MISO GPIO_NUM_37
#define PIN_SCLK GPIO_NUM_36
#define PIN_CS GPIO_NUM_34
#elif CONFIG_IDF_TARGET_ESP32H2
#define PIN_MOSI GPIO_NUM_5
#define PIN_MISO GPIO_NUM_0
#define PIN_SCLK GPIO_NUM_4
#define PIN_CS GPIO_NUM_1
#else
#define PIN_MOSI GPIO_NUM_4
#define PIN_MISO GPIO_NUM_6
#define PIN_SCLK GPIO_NUM_5
#define PIN_CS GPIO_NUM_1
#endif

#define DATA_FILE_PRE_NAME "sniffer-data"
#define MOUNT_POINT "/sdcard"

void init_sdcard();

void write_file(const char *path, char *data);

void read_file(const char *path);

void stop_sdcard();

#endif //!__SDCARD__H__