#include "wifi_manager.h"
#include <stdio.h>

#define TAG "wifi"
#define MAX_CHAN 13

static EventGroupHandle_t s_wifi_event_group;
esp_mqtt_client_handle_t global_client; // Cliente MQTT

TaskHandle_t SnifferTask;

void (*mqtt_callback)(char *, char *, unsigned int,
                      unsigned int); // Função de callback
void (*sniffer_callback)(void *,
                         wifi_promiscuous_pkt_type_t); // Função de callback

const wifi_promiscuous_filter_t filt =
    { // Filtro dos pacotes a serem interceptados
        .filter_mask =
            WIFI_PROMIS_FILTER_MASK_MGMT | WIFI_PROMIS_FILTER_MASK_DATA};

static int currentChannel = 1; // Canal atual de operação

int s_retry_num = 0; // Contador de tentativas de conexão

void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id,
                   void *event_data) {
    if (event_base == WIFI_EVENT &&
        event_id ==
            WIFI_EVENT_STA_START) { // Verifica se o evento e de WiFi é se e do
                                    // tipo de inicialização de estação
        esp_wifi_connect();

    } else if (event_base == WIFI_EVENT &&
               event_id ==
                   WIFI_EVENT_STA_DISCONNECTED) { // Verifica se o evento e de
                                                  // WiFi é se do tipo de
                                                  // desconexão

        if (s_retry_num <
            maximum_retry) { // Verifica se o numero de tentativas de reconexã
                             // ainda não atingiu o limite
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG,
                     "Tentando conectar ao ponto de acesso"); // Log de debug
        } else { // Caso contrario toma as medidas necessarias
            ESP_LOGE(
                TAG,
                "Não foi possível conectar ao ponto de acesso"); // Log de debug

            xEventGroupSetBits(s_wifi_event_group,
                               WIFI_FAIL_BIT); // Envia um sinal de falha e o
                                               // dispositivo reinicia
            esp_restart();
        }
        ESP_LOGI(TAG, "connect to the AP fail");

    } else if (event_base == IP_EVENT &&
               event_id == IP_EVENT_STA_GOT_IP) { // Verifica se o evento de
                                                  // obtenção de IP
        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;

        ESP_LOGI(TAG, "Obteve o IP:" IPSTR, IP2STR(&event->ip_info.ip));

        s_retry_num = 0; // Coloca o numero de tentativas como 0, pois a conexão
                         // foi estabelecida

        xEventGroupSetBits(s_wifi_event_group,
                           WIFI_CONNECTED_BIT); // Envia um sinal de conexão
    }
}

void init_wifi() {
    ESP_LOGI(TAG, "Inicinado WiFi");
    esp_err_t ret =
        nvs_flash_init(); // Inicia a memoria NVS (Responsavel por salvar
                          // valores de forma criptografada na flash)
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES ||
        ret == ESP_ERR_NVS_NEW_VERSION_FOUND) // Caso a NVS não esteja alocada,
                                              // ou esteja cheia ele a formata
    {
        ESP_ERROR_CHECK(nvs_flash_erase()); // Formata a NVS
        ret = nvs_flash_init();             // Inicia a NVS
    }
    ESP_ERROR_CHECK(ret);
    s_wifi_event_group =
        xEventGroupCreate(); // Cria o gerenciador de eventos globais do WiFi

    ESP_ERROR_CHECK(esp_netif_init()); // Inicia o netif

    ESP_ERROR_CHECK(esp_event_loop_create_default());

    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg =
        WIFI_INIT_CONFIG_DEFAULT(); // Inicia uma configuração padrão do WiFi
    ESP_ERROR_CHECK(esp_wifi_init(&cfg)); // Inicia o wifi com essa configuração

    esp_event_handler_instance_t
        instance_any_id; // Cria um manajer para eventos diversos do WiFi
    esp_event_handler_instance_t
        instance_got_ip; // Cria um manajer para eventos para lidar com eventos
                         // de endereço IP

    ESP_ERROR_CHECK(esp_event_handler_instance_register(
        WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL, &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(
        IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL, &instance_got_ip));

    esp_wifi_set_storage(
        WIFI_STORAGE_RAM); // Configura para o wifi utilizar a memoria ram para
                           // realizar qualquer tipo de armazenamento de dados
}

void init_sniffer() {
    esp_wifi_set_mode(
        WIFI_MODE_NULL); // Deixa o WiFi configurado em nenhum modo, pois ele
                         // não se conectara a nada e nem iniciara um hostpost
    esp_wifi_start();    // Starta o WiFi
    esp_wifi_set_promiscuous(true); // Ativa a interceptação de pacotes
    esp_wifi_set_promiscuous_filter(
        &filt); // Configura um filtro para interceptação dos pacotes
    esp_wifi_set_promiscuous_rx_cb(
        sniffer_callback); // Seta a função responsavel por se comportar
                           // como callback
    esp_wifi_set_channel(
        currentChannel,
        WIFI_SECOND_CHAN_NONE); // Configura o canal em que o WiFi ira operar
}

void stop_sniffer() {
    // esp_wifi_stop();
    esp_wifi_set_promiscuous(false);
}

void init_wifi_sta() {
    wifi_config_t wifi_config = {
        .sta =
            {
                .ssid = WIFI_SSID,
                .password = WIFI_PASS,
                .threshold.authmode = WIFI_AUTH_WPA2_PSK,
                .sae_pwe_h2e = WPA3_SAE_PWE_BOTH,
            },
    };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
                                           WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                                           pdFALSE, pdFALSE, portMAX_DELAY);

    // xEventGroupWaitBits() retorna os bits antes do retorno da chamada,
    // portanto pode testar qual evento realmente aconteceu.

    ESP_LOGI(TAG,
             "Agurdando verificação de inicialização do WiFi"); // Log de debug

    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "conectado a rede com SSID: %s", "INFO_PREF");
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Falha ao tentar conectar ao SSID: %s", "INFO_PREF");
    } else {
        ESP_LOGE(TAG, "Evento inesperado");
    }

    ESP_LOGI(TAG,
             "Processo de inicialização completo"); // Log de debug
}

void stop_sta() {
    // esp_netif_deinit();
    esp_wifi_disconnect();
    // esp_wifi_stop();
    esp_wifi_set_mode(WIFI_MODE_NULL);
}

void reinit_sta() {
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));

    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
                                           WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                                           pdFALSE, pdFALSE, portMAX_DELAY);

    // xEventGroupWaitBits() retorna os bits antes do retorno da chamada,
    // portanto pode testar qual evento realmente aconteceu.

    ESP_LOGI(TAG,
             "Agurdando verificação de inicialização do WiFi"); // Log de debug

    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "conectado a rede com SSID: %s", "INFO_PREF");
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Falha ao tentar conectar ao SSID: %s", "INFO_PREF");
    } else {
        ESP_LOGE(TAG, "Evento inesperado");
    }

    ESP_LOGI(TAG,
             "Processo de inicialização completo"); // Log de debug
}

void sniffer_task(void *arg) {
    vTaskDelay(1000 / portTICK_PERIOD_MS);

    while (1) // Executa infinitamente
    {
        if (currentChannel >
            MAX_CHAN) // Verica se o canal atual e maior que o limite
        {
            currentChannel = 1; // Se sim reinicia a contagem
        }
        esp_wifi_set_channel(currentChannel,
                             WIFI_SECOND_CHAN_NONE); // Seta o novo canal

        vTaskDelay(pdMS_TO_TICKS(1000)); // Espera um segundo
        currentChannel++;                // Inclementa mais um no canal
    }

    vTaskDelete(NULL);
}

void mqtt_event_handler(void *handler_args, esp_event_base_t base,
                        int32_t event_id, void *event_data) {
    // ESP_LOGD(TAG, "Event dispatched from event loop base=%s,
    // event_id=%d", base, event_id);
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    switch ((esp_mqtt_event_id_t)event_id) { // Verifica o tipo de evento
    case MQTT_EVENT_CONNECTED:               // Conexão realizada
        ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");

        msg_id = esp_mqtt_client_publish(
            client, "node_1", "conectado", 0, 1,
            0); // Envia uma mensagem para informar a conexão
        ESP_LOGI(TAG, "mensagem publicada com sucesso, msg_id=%d", msg_id);

        char temp_mqtt_topic[148]; // Buffer para armazenar o topico final

        strlcpy(temp_mqtt_topic, "node_1",
                sizeof(temp_mqtt_topic)); // Copio o topico da configuração para
                                          // o buffer temporario
        strlcat(temp_mqtt_topic, "receive/",
                sizeof(temp_mqtt_topic) - 1); // concateno receive/ no final

        msg_id = esp_mqtt_client_subscribe(client, temp_mqtt_topic,
                                           0); // Subscrição ao topico
        ESP_LOGI(TAG, "enviado inscrição com sucesso, msg_id=%d",
                 msg_id); // Log de debug com o id da mensagem

        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");

        break;

    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d",
                 event->msg_id); // Log de debug com o id da mensagem
        msg_id = esp_mqtt_client_publish(client, "node_1", "conectado", 0, 0,
                                         0); // Publica mensagem no topico
        ESP_LOGI(TAG, "sent publish successful, msg_id=%d",
                 msg_id); // Log de debug com o id da mensagem
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        if (!mqtt_callback) {
            ESP_LOGI(TAG, "MQTT_EVENT_DATA");
            printf("TOPIC=%.*s\r\n", event->topic_len,
                   event->topic); // Log de debug com o topico
            printf("DATA=%.*s\r\n", event->data_len,
                   event->data); // Log de debug com o payload
        } else {
            mqtt_callback(
                event->topic, event->data, event->topic_len,
                event->data_len); // Chama o callback de recebimento de mensagem
        }
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
        if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT) {
            // log_error_if_nonzero("reported from esp-tls",
            //                      event->error_handle->esp_tls_last_esp_err);
            // log_error_if_nonzero("reported from tls stack",
            //                      event->error_handle->esp_tls_stack_err);
            // log_error_if_nonzero("captured as transport's socket errno",
            //                      event->error_handle->esp_transport_sock_errno);
            ESP_LOGI(TAG, "Last errno string (%s)",
                     strerror(event->error_handle->esp_transport_sock_errno));
        }
        break;
    default:
        ESP_LOGI(TAG, "Erro desconhecido, id do erro:%d", event->event_id);
        break;
    }
}

void mqtt_start(void) // Inicia o MQTT
{

    ESP_LOGI(TAG, "Configurando MQTT"); // Log de debug

    ESP_LOGI(TAG, "MQTT: conectando ao host %s",
             "localhost"); // Log de debug

    const esp_mqtt_client_config_t mqtt_cfg = {
        .broker.address.uri = MQTT_URI,                  // URI do servidor
        .credentials.username = MQTT_USER,               // Usuário do servidor
        .credentials.authentication.password = MQTT_PASS // Senha do servidor
    }; // Configurações do MQTT

    global_client = esp_mqtt_client_init(&mqtt_cfg); // Configura o cliente MQTT

    ESP_LOGI(TAG,
             "Configuração do MQTT terminada"); // Log de debug

    esp_mqtt_client_register_event(global_client, ESP_EVENT_ANY_ID,
                                   mqtt_event_handler,
                                   NULL); // Registra o callback de eventos
    esp_mqtt_client_start(global_client); // Inicia o cliente MQTT

    ESP_LOGI(TAG, "MQTT, iniciado");
}

int send_message(char *message) {

    // Envia mensagem
    ESP_LOGI(TAG, "Enviando mensagem ao topico %s: %s", "node_1",
             message); // Log de debug

    int msg_id = esp_mqtt_client_publish(global_client, "node_1", message, 0, 0,
                                         0); // Envia a mensagem

    ESP_LOGI(TAG, "Mensagem enviada com sucesso, msg_id=%d",
             msg_id); // Log de debug

    return msg_id; // Retorna o id da mensagem
}

/**
 * @brief Função responsavel por parar o MQTT
 *
 * @return esp_err_t Resultado ao parar MQTT
 */
esp_err_t stop_mqtt(void) {                     // Para o MQTT
    return esp_mqtt_client_stop(global_client); // Para o cliente MQTT
}

esp_err_t start_mqtt(void) {
    return esp_mqtt_client_start(global_client); // Inicia o cliente MQTT
}

void setcallback(void (*_callback)(char *, char *, unsigned int,
                                   unsigned int)) {
    mqtt_callback = _callback;
}
void set_sniffercallback(void (*_callback)(void *,
                                           wifi_promiscuous_pkt_type_t)) {
    sniffer_callback = _callback;
}

void init_sniffer_task() {
    xTaskCreate(sniffer_task, "sniffer_task", 8192, NULL, 8, &SnifferTask);
}

void suspend_sniffer_task() { vTaskSuspend(SnifferTask); }

void resume_sniffer_task() { vTaskResume(SnifferTask); }