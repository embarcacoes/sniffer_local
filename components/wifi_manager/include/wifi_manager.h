#ifndef __WIFI_MANAGER__H__
#define __WIFI_MANAGER__H__

#include "esp_err.h"
#include "esp_event.h" // Biblioteca de Eventos
#include "esp_log.h"
#include "esp_mac.h"
#include "esp_netif.h"
#include "esp_system.h"
#include "esp_wifi.h" // Biblioteca do Wifi
#include "esp_wifi_types.h"
#include "freertos/FreeRTOS.h" // FreeRTOS
#include "freertos/event_groups.h"
#include "freertos/portmacro.h"
#include "freertos/projdefs.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h" // Task
#include "mqtt_client.h"   // Biblioteca MQTT
#include "nvs.h"
#include "nvs_flash.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define WIFI_CONNECTED_BIT BIT0 // Bit 0 - Wifi conectado
#define WIFI_FAIL_BIT BIT1      // Bit 1 - Falha na conexão
#define maximum_retry 50
#define MQTT_URI ""
#define MQTT_USER ""
#define MQTT_PASS ""
#define WIFI_SSID ""
#define WIFI_PASS ""

void setcallback(void (*_callback)(char *, char *, unsigned int, unsigned int));
void mqtt_start(void);
int send_message(char message[]);
esp_err_t stop_mqtt(void); // Função para parar o MQTT
void init_wifi();
void init_sniffer();
void stop_sniffer();
void init_wifi_sta();
void stop_sta();
void reinit_sta();
esp_err_t start_mqtt(void);
void setcallback(void (*_callback)(char *, char *, unsigned int, unsigned int));
void set_sniffercallback(void (*_callback)(void *,
                                           wifi_promiscuous_pkt_type_t));
void init_sniffer_task();
void suspend_sniffer_task();
void resume_sniffer_task();

#endif //!__WIFI_MANAGER__H__