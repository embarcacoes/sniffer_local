#include "config.h"
#include "driver/gpio.h"
#include "esp_event.h" // Biblioteca de Eventos
#include "esp_log.h"   // Biblioteca de Log
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "freertos/task.h" // Task
#include "sdcard.h"
#include "time_manager.h"
#include "wifi_manager.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char *TAG = "sniffer"; // Tag de LOG

static SemaphoreHandle_t SEND_DATA_SEMAPHORE = NULL;
static SemaphoreHandle_t SD_Semaphore = NULL;

FILE *data_file = NULL;

TaskHandle_t DataTask;
TaskHandle_t WriteTask;

QueueHandle_t send_data_queue; // Fila de comunicação entre as tasks de
                               // sniffer e envio de dados

void sniffer(void *buf, wifi_promiscuous_pkt_type_t type);

void sniffer_task(void *arg);
void send_data_task(void *pvargs);
void saveTask(void *pvargs);

void app_main(void) {
    ESP_LOGI(TAG, "Iniciando sistema...");

    init_sdcard();

    vTaskDelay(1000 / portTICK_PERIOD_MS);

    SEND_DATA_SEMAPHORE = xSemaphoreCreateBinary();

    if (SEND_DATA_SEMAPHORE == NULL) {
        ESP_LOGE(TAG, "Erro ao iniciar semaforo");
        esp_restart();
    } else {
        ESP_LOGI(TAG, "Semaforo iniciado com sucesso");
    }

    SD_Semaphore = xSemaphoreCreateBinary();

    if (SD_Semaphore == NULL) {
        ESP_LOGE(TAG, "Erro ao iniciar semaforo, reiniciando sistema.");
        stop_sdcard();
        esp_restart();
    } else {
        ESP_LOGI(TAG, "Semaforo iniciado");
    }

    xSemaphoreGive(SD_Semaphore); // Libera semaforo

    send_data_queue = xQueueCreate(
        SEND_DATA_QUEUE_SIZE,
        sizeof(char) * TX_BUF_SIZE); // Cria uma fila de envio de dados

    set_sniffercallback(&sniffer);

    init_wifi();

    init_wifi_sta();

    vTaskDelay(1000 / portTICK_PERIOD_MS);

    mqtt_start(); // Inicialização do servidor
                  // MQTT

    init_time_manager();

    vTaskDelay(1000 / portTICK_PERIOD_MS);

    stop_mqtt();

    xTaskCreate(send_data_task, "Send data task", 8192, NULL, 10, &DataTask);
    xTaskCreate(saveTask, "save data task", 8192, NULL, 10, &WriteTask);

    vTaskDelay(1000 / portTICK_PERIOD_MS);

    stop_sta();

    vTaskDelay(1000 / portTICK_PERIOD_MS);

    init_sniffer();

    vTaskDelay(1000 / portTICK_PERIOD_MS);

    init_sniffer_task();

    vTaskSuspend(DataTask);

    ESP_LOGI(TAG, "Rotina de inicialização concluida...");

    vTaskDelay(60000 / portTICK_PERIOD_MS);

    vTaskResume(DataTask);
    xSemaphoreGive(SEND_DATA_SEMAPHORE);

    vTaskDelay(60000 / portTICK_PERIOD_MS);

    xSemaphoreGive(SEND_DATA_SEMAPHORE);
}

void sniffer(void *buf, wifi_promiscuous_pkt_type_t type) {
    /*
    Função responsavel por atual como callback de recepção dos pacotes
    interceptados. É aqui que os pacotes terminam depois de serem detectados
    */
    wifi_promiscuous_pkt_t *p =
        (wifi_promiscuous_pkt_t *)buf; // Converte o buffer recebido para um
                                       // struct do tipo wifi_promiscuous_pkt_t
    int len = p->rx_ctrl.sig_len;      // Obtem o tamanho do pacote recebido

    if (len <= 0) // Verifica se o tamanho e menor e igual a 0, caso se encaixe
                  // nesse padrão o pacote e invalido
    {
        ESP_LOGI(TAG, "Recebido 0");
        return; // Sai da função
    }

    char data[30] = {0}; // Aloca uma string para colocar os dados a serem
                         // enviados

    memset(data, 0, sizeof(data));

    sprintf(data, "%02X%02X%02X%02X%02X%02X,%d", p->payload[10], p->payload[11],
            p->payload[12], p->payload[13], p->payload[14], p->payload[15],
            p->rx_ctrl.rssi); // Coloca o mac do pacote detectado e o RSSI do
                              // mesmo separado or virgula

    // printf("Recebido: %s \n", data);

    xQueueSend(
        send_data_queue, (void *)data,
        (TickType_t)0); // Coloca os dados na fila para serem envidos via UART
}

void saveTask(void *pvargs) {
    char buffer[30] = {0};
    char time_buffer[60];

    while (1) {
        if (send_data_queue == NULL || SD_Semaphore == NULL)
            break;

        if (xQueueReceive(send_data_queue, buffer, portMAX_DELAY)) {
            while (1) {
                if (xSemaphoreTake(SD_Semaphore, pdMS_TO_TICKS(200)) == false) {
                    vTaskDelay(100 / portTICK_PERIOD_MS);
                    continue;
                }

                get_time_str(time_buffer);
                // printf("time: %s\n", time_buffer);
                data_file = fopen("/sdcard/data.txt", "a+");
                printf("salvando\n");
                fputs(buffer, data_file);
                fputc(',', data_file);
                fputs(time_buffer, data_file);
                fputc('\n', data_file);

                fclose(data_file);

                xSemaphoreGive(SD_Semaphore);

                break;
            }
        } else {
            ESP_LOGI(TAG, "Fila vazia... aguardando 2s extras");
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }

        vTaskDelay(25 / portTICK_PERIOD_MS);
    }

    fclose(data_file);
    data_file = NULL;

    vTaskDelete(NULL);
}

void send_data_task(void *pvargs) {
    ESP_LOGI(TAG, "Iniciando task de envio de dados");

    vTaskDelay(1000 / portTICK_PERIOD_MS);

    while (1) {
        if (SEND_DATA_SEMAPHORE == NULL)
            break;

        if (xSemaphoreTake(SEND_DATA_SEMAPHORE, pdMS_TO_TICKS(2000)) == true) {

            while (1) {
                if (xSemaphoreTake(SD_Semaphore, pdMS_TO_TICKS(100)) == false) {
                    vTaskDelay(100 / portTICK_PERIOD_MS);
                    continue;
                }

                suspend_sniffer_task();
                vTaskSuspend(WriteTask);
                stop_sniffer();

                vTaskDelay(2000 / portTICK_PERIOD_MS);

                reinit_sta();

                vTaskDelay(10000 / portTICK_PERIOD_MS);

                start_mqtt();

                vTaskDelay(10000 / portTICK_PERIOD_MS);

                ESP_LOGI(TAG, "Enviando dados");

                if (data_file != NULL)
                    fclose(data_file);

                data_file = fopen("/sdcard/data.txt", "r");

                char buffer[144];

                while (fgets(buffer, 144, data_file)) {
                    send_message(buffer);
                    vTaskDelay(5 / portTICK_PERIOD_MS);
                }

                fclose(data_file);

                remove("/sdcard/data.txt");

                data_file = NULL;

                stop_mqtt();

                vTaskDelay(5000 / portTICK_PERIOD_MS);

                xSemaphoreGive(SD_Semaphore);
                stop_sta();

                vTaskDelay(1);

                init_sniffer();

                vTaskResume(WriteTask);
                resume_sniffer_task();

                break;
            }
        } else {
            vTaskDelay(100 / portTICK_PERIOD_MS);
        }
    }

    vTaskDelete(NULL);
}
