#ifndef __CONFIG__H__
#define __CONFIG__H__

#include "driver/gpio.h"
#include "sdkconfig.h"

#define TX_BUF_SIZE 25          // Tamanho do buffer de escrita via UART
#define SEND_DATA_QUEUE_SIZE 60 // Tamanho da fila de dados a serem enviados
#define MOUNT_POINT "/sdcard"
#define MAX_CHAR_SIZE 64

#endif //!__CONFIG__H__